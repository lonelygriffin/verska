$(document).ready(function () {
 $('.stepper').mdbStepper();
})

var metro = [
	"Авиамоторная",
	"Автозаводская",
	"Академическая",
	"Александровский сад",
	"Алексеевская",
	"Алма-Атинская",
	"Алтуфьево",
	"Аннино",
	"Арбатская",
	"Аэропорт",
	"Выхино"
];

$('#form-autocomplete').mdb_autocomplete({
	data: metro
});

// Data Picker Initialization
$('.datepicker').pickadate();